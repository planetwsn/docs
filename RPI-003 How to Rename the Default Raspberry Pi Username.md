**How to Rename the Default Raspberry Pi Username**

Renaming a username requires renaming the same to all important documents in RPi. As such, enough care to backup files and careful typing of the following instructions must be done. Make sure that you copy the instructions exactly - down to even spaces and punctuation marks.

* `sudo useradd -m tempuser` (create a new temporary username)
* `sudo passwd tempuser` (give that username a password)

You will be prompted to input a password and to confirm it by retyping it.

* `sudo usermod -a -G sudo tempuser` (add tempuser account to the group named *sudo*)

The reason for creating a new temporary file is because you will be moving all your current's files and permissions to the new username and you cannot do so if you're logged in to the former account.

* `logout`

Login to `tempuser` with the password you created earlier. Expect changes in terminal layout among other things.

* `cd /etc` 

When you do this, the terminal may sometimes fail to show which directory you are currently at, because your `.bashrc` file has not been configured for this username.

* `sudo tar -cvf authfiles.tar passwd group gshadow sudoers lightdm/lightdm.conf systemd/system/autologin@.service sudoers.d/* polkit-1/localauthority.conf.d/60-desktop-policy.conf`

If you running a headless RPi (no desktop nor GUI), do not include `lightdm/lightdm.conf` and `polkit-1/localauthority.conf.d/60-desktop-policy.conf` in the previous instruction.

What we did in the previous step was to backup critical files, so that if something goes wrong, you can still save your RPi by extracting these files back.

* `sudo sed -i.$(date+'%y%m%d_%H%M%S') 's/\bpi\b/newname/g' passwd group gshadow sudoers lightdm/lightdm.conf systemd/system/autologin@.service sudoers.d/* polkit-1/localauthority.conf.d/60-desktop-policy.conf`

The previous instructions replaces all `pi` usernames with `newname`. Feel free to choose your desired username and replace `newname` in the code above. If you are renaming something else other than `pi`. Replace that name with `pi` above.

* `grep newname /etc/group` (check if changes were made)
* `sudo mv /home/pi /home/newname` (move all files inside `pi` to `newname`)
* `sudo ln -s /home/newname /home/pi` (create symbolic links to `newname` from `pi`)

Again, if you're renaming an account other than `pi`, replace `pi` here.

* `sudo passwd newname`

Logout and then login to your new username. You may delete the temporary username by
* `sudo userdel tempuser`

You may also want to delete the replaced username's directory at /home.

* `sudo rm -R /home/pi`

You're done!

